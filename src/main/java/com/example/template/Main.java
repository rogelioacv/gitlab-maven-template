package com.example.template;

/**
 * This is the entry point of the application.
 *
 * @since 1.0.0
 * @author timothystone
 */
public class Main {

  public static void main(String[] args) {
    System.out.println("Maven Test.");
  }
}
